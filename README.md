# Hipventory PHP bindings

You can sign up for a Hipventory account at https://www.hipventory.com/.

## Manual Installation

You can download the [latest release](https://github.com/hipventory/hipventory-php/releases). Then, to use the bindings, include the `autoload.php` file.

    require_once('/path/to/hipventory-php/autoload.php');

## Getting Started

Initialize with your token:

    $hipventory = new Swagger\Client\Api\OrdersApi();
    $hipventory->getApiClient()->getConfig()->addDefaultHeader("Authorization", "Token {your_token}");

## Documentation

Please see http://docs.hipventory.com/ for up-to-date documentation.
